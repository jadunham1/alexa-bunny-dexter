var APP_ID = "amzn1.echo-sdk-ams.app.998627c5-c74a-45ea-930c-3e569eb71836";

/**
 * Array containing dexter activities.
 */
var DEXTER_ACTIVITIES = [
    "I'm hoping this works, round two!",
    "I'm going to the bathroom all over your floor!",
    "I'm pooping!",
    "I'm waiting for someone to pet me",
    "I'm being adorable",
    "None of your business!",
    "You should worry about you, I'll worry about me",
    "I'm eating hay",
    "I'm tearing up the carpet",
    "Wishing you would stop asking me what I'm doing",
    "Minding my own business",
    "I'm drinking water",
    "I'm cudding next to my favorite people",
    "I'm praying someone doesn't take my balls away",
    "Wondering what outside is like",
    "What do you think I'm doing?  I'm a bunny",
    "I'm shedding all of my fur",
    "I'm wondering how many times you're going to ask me what I'm doing",
    "I'm hopping around",
    "I'm waiting for someone to clean out my cage",
    "I'm sleeping leave me alone!",
    "Nothing to see here, move on!",
    "I'm confused at how Alexa is able to understand what I'm saying",
    "I'm sharpening my teeth, watch out!",
    "I'm sharpening my nails, watch out!",
    "I'm peeing in the corner",
    "I'm searching the internet for how to get out of this cage!",
    "I'm attempting an escape",
    "I've escaped!",
    "I'm waiting for true love",
    "I'm a dude, he's a dude, she's a dude, we're all dudes",
    "I am watching where you clean so I know where to poop next",
    "I am dreaming about running in the open air",
    "Definitely not holding my breath for the hutch you promised me",
    "DEE DEE!"
];

/**
 * Array containing dexter moods.
 */


var DEXTER_MOODS = [
    "I'm doing great!",
    "I've been better",
    "Perfect!",
    "Never been better!",
    "I'm feeling loved",
    "I'm feeling dangerous",
    "Despite all my rage I'm still just a bunny in a cage",
    "I'm feeling saucy",
    "I'm feeling scared",
    "I'm wonderful, thanks for asking",
    "It's a wonderful day to be a bunny",
    "I am feeling listless",
    "A little derpy",
    "I poop, therefore, I am",
    "I feel nothing", 
    "I am dead inside",
    "La dee da, I feel like singing",
    "I feel like my number is up and John Reese is coming to get me"
    
];

/**
 * The AlexaSkill prototype and helper functions
 */
var AlexaSkill = require('./AlexaSkill');

/**
 * AlexaBunnyDexter is a child of AlexaSkill.
 * To read more about inheritance in JavaScript, see the link below.
 *
 * @see https://developer.mozilla.org/en-US/docs/Web/JavaScript/Introduction_to_Object-Oriented_JavaScript#Inheritance
 */
var AlexaBunnyDexter = function () {
    AlexaSkill.call(this, APP_ID);
};

// Extend AlexaSkill
AlexaBunnyDexter.prototype = Object.create(AlexaSkill.prototype);
AlexaBunnyDexter.prototype.constructor = AlexaBunnyDexter;

AlexaBunnyDexter.prototype.eventHandlers.onSessionStarted = function (sessionStartedRequest, session) {
    console.log("AlexaBunnyDexter onSessionStarted requestId: " + sessionStartedRequest.requestId
        + ", sessionId: " + session.sessionId);
};

AlexaBunnyDexter.prototype.eventHandlers.onLaunch = function (launchRequest, session, response) {
    console.log("AlexaBunnyDexter onLaunch requestId: " + launchRequest.requestId + ", sessionId: " + session.sessionId);
    handleDexterActivityRequest(response);
};

/**
 * Overridden to show that a subclass can override this function to teardown session state.
 */
AlexaBunnyDexter.prototype.eventHandlers.onSessionEnded = function (sessionEndedRequest, session) {
    console.log("AlexaBunnyDexter onSessionEnded requestId: " + sessionEndedRequest.requestId);
};

AlexaBunnyDexter.prototype.intentHandlers = {
    GetDexterActivityIntent: function (intent, session, response) {
        handleDexterActivityRequest(response);
    },
    GetDexterMoodIntent: function (intent, session, response) {
        handleDexterMoodRequest(response);
    },
    HelpIntent: function (intent, session, response) {
        response.ask("You can ask Dexter how he is feeling, what he is doing, or, you can say exit... ");
    }
};

/**
 * Gets a random activity from DEXTER_ACTIVITIES list.
 */
function handleDexterActivityRequest(response) {
    var dexActivitiesIndex = Math.floor(Math.random() * DEXTER_ACTIVITIES.length);
    var dexActivity = DEXTER_ACTIVITIES[dexActivitiesIndex];

    // Create speech output
    var speechOutput = "Dexter says: " + dexActivity;

    response.tellWithCard(speechOutput, "AlexaBunnyDexter", speechOutput);
}

/**
 * Gets a random activity from DEXTER_MOODS list.
 */
function handleDexterMoodRequest(response) {
    var dexMoodsIndex = Math.floor(Math.random() * DEXTER_MOODS.length);
    var dexMood = DEXTER_MOODS[dexMoodsIndex];
    
    // Create speech output
    var speechOutput = "Dexter says: " + dexMood;
    
    response.tellWithCard(speechOutput, "AlexaBunnyDexter", speechOutput);
}

// Create the handler that responds to the Alexa Request.
exports.handler = function (event, context) {
    // Create an instance of the AlexaBunnyDexter skill.
    var dexter = new AlexaBunnyDexter();
    dexter.execute(event, context);
};
